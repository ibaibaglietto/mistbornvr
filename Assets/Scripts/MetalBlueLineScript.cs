using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OculusSampleFramework
{
    public class MetalBlueLineScript : MonoBehaviour
    {

        public enum CrosshairState { Disabled, Enabled, Targeted }

        CrosshairState m_state = CrosshairState.Disabled;
        Transform m_center;

        [SerializeField] GameObject m_targetedCrosshair = null;
        [SerializeField] GameObject m_enabledCrosshair = null;


        private void Start()
        {
            m_center = GameObject.Find("CenterEyeAnchor").transform;
        }
        public void SetState(CrosshairState cs)
        {
            m_state = cs;
            if (cs == CrosshairState.Disabled)
            {
                m_targetedCrosshair.SetActive(false);
                m_enabledCrosshair.SetActive(false);
            }
            else if (cs == CrosshairState.Enabled)
            {
                m_targetedCrosshair.SetActive(false);
                m_enabledCrosshair.SetActive(true);
            }
            else if (cs == CrosshairState.Targeted)
            {
                m_targetedCrosshair.SetActive(true);
                m_enabledCrosshair.SetActive(false);
            }
        }

        private void Update()
        {
            if (m_state != CrosshairState.Disabled)
            {
                Vector3 center = new Vector3(m_center.position.x, m_center.position.y - 0.4f, m_center.position.z);
                transform.LookAt(center);
                transform.localScale = new Vector3(transform.localScale.x , transform.localScale.y , Vector3.Distance(center, transform.position)/2.0f);
            }
        }
    }
}
