using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVRTouchSample;

namespace OculusSampleFramework
{
    public class GrabbableMetalScript : OVRGrabbable
    {
        public string m_materialColorField;

        MetalBlueLineScript m_crosshair;
        MetalManagerScript m_crosshairManager;
        Renderer m_renderer;
        MaterialPropertyBlock m_mpb;
        //A boolean to know if the metal has the pivot on the parent
        [SerializeField] private bool hasParent;
        //A boolean to know if the metal is heavier than the player
        [SerializeField] private bool heavy;
        //An int to know if a light object is being pushed against a wall/floor
        private int pushFloor = 0;


        public bool InRange
        {
            get { return m_inRange; }
            set
            {
                m_inRange = value;
                RefreshCrosshair();
            }
        }
        bool m_inRange;

        public bool Targeted
        {
            get { return m_targeted; }
            set
            {
                m_targeted = value;
                RefreshCrosshair();
            }
        }
        bool m_targeted;

        protected override void Start()
        {
            base.Start();
            m_crosshair = gameObject.GetComponentInChildren<MetalBlueLineScript>();
            m_renderer = gameObject.GetComponent<Renderer>();
            m_crosshairManager = FindObjectOfType<MetalManagerScript>();
            m_mpb = new MaterialPropertyBlock();
            RefreshCrosshair();
            m_renderer.SetPropertyBlock(m_mpb);
        }

        public bool HasParent()
        {
            return hasParent;
        }

        public bool IsHeavy()
        {
            return heavy;
        }

        public bool IsBeingPushed()
        {
            return pushFloor>0 && !heavy;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Floor")
            {
                pushFloor += 1;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Floor") pushFloor -= 1;
        }

        void RefreshCrosshair()
        {
            if (m_crosshair)
            {
                if (isGrabbed) m_crosshair.SetState(MetalBlueLineScript.CrosshairState.Disabled);
                else if (!InRange) m_crosshair.SetState(MetalBlueLineScript.CrosshairState.Disabled);
                else m_crosshair.SetState(Targeted ? MetalBlueLineScript.CrosshairState.Targeted : MetalBlueLineScript.CrosshairState.Enabled);
            }
            if (m_materialColorField != null)
            {
                m_renderer.GetPropertyBlock(m_mpb);
                if (isGrabbed || !InRange) m_mpb.SetColor(m_materialColorField, m_crosshairManager.OutlineColorOutOfRange);
                else if (Targeted) m_mpb.SetColor(m_materialColorField, m_crosshairManager.OutlineColorHighlighted);
                else m_mpb.SetColor(m_materialColorField, m_crosshairManager.OutlineColorInRange);
                m_renderer.SetPropertyBlock(m_mpb);
            }
        }
    }
}
