using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OculusSampleFramework
{
    public class MetalManagerScript : MonoBehaviour
    {
        Collider m_grabVolume;

        public Color OutlineColorInRange;
        public Color OutlineColorHighlighted;
        public Color OutlineColorOutOfRange;

        void OnTriggerEnter(Collider otherCollider)
        {
            GrabbableMetalScript dg = otherCollider.GetComponentInChildren<GrabbableMetalScript>();
            if (dg)
            {
                dg.InRange = true;
            }

        }

        void OnTriggerExit(Collider otherCollider)
        {
            GrabbableMetalScript dg = otherCollider.GetComponentInChildren<GrabbableMetalScript>();
            if (dg)
            {
                dg.InRange = false;
            }
        }
    }
}
